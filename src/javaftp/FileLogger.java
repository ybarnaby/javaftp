/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaftp;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
  
public class FileLogger{
    final static Logger LOGGER = Logger.getLogger("MyLog");
    static FileHandler fh;
    private static final String FILE_LOCATION = "C:/outputs/log/JavaFTP-log.txt";

    public static void log(String pattern) {
        try {
            // This block configure the LOGGER with handler and formatter
            fh = new FileHandler(FILE_LOCATION, true);
            LOGGER.addHandler(fh);
            LOGGER.setLevel(Level.FINE);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            // the following statement is used to log any messages
            LOGGER.log(Level.INFO, pattern);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //logger.info("Hi How r u?");
    }
    
    public static void bLog(String pattern) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(FILE_LOCATION, true));

            //Set the date format.
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            //Get the current date according to local settings.
            String dateAsString = simpleDateFormat.format(new Date());

            //Write information into file and create a new line.
            bw.write(dateAsString +": "+ String.format(pattern));
            bw.newLine();
            bw.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
    
//    public static void main(String[] args) {
//        FileLogger f = new FileLogger();
//       //f.log("S");
//    }
}
