/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaftp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ybarnaby
 */
public class mySQLConnectors {

    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    //for logging
    private static final Logger logger = Logger.getLogger(JavaFTP.class.getName());
    FileLogger logging = new FileLogger();

    /**
     *
     * @return connection to be used all over
     */
    public Connection mysqlConnect() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            FileLogger.log("Connected to Database");
            return connect = DriverManager.getConnection("jdbc:mysql://app2/diinfo?" + "user=ybarnaby&password=!");

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(JavaFTP.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param filename
     * @param tablesuffix
     * @param prefix
     *
     */
    public void loadDataInfile(String filename, String tablesuffix, String prefix) {
        try {
            System.out.printf(filename + ": " + tablesuffix + " and " + prefix + "\n");
            if (!checkIsNull(filename, tablesuffix)) {
                String sql = "load data local infile 'C:/outputs/files/yoox/" + filename + "' "
                        + "into table " + tablesuffix + " "
                        + "fields terminated by ',' "
                        + "lines terminated by '\\r\\n' "
                        + "ignore 1 lines;";
                preparedStatement = mysqlConnect().prepareStatement(sql);
                resultSet = preparedStatement.executeQuery();
                //reseting the strings to null for the next loop
                filename = "";
                tablesuffix = "";
                FileLogger.log("File stored to DB table: " + tablesuffix);
            } else {
                System.err.print(filename + " does not have a key or key is not being accessed.\n");
            }
        } catch (SQLException ex) {
            Logger.getLogger(JavaFTP.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param filename
     * @param tablesuffix returns only a couples of files
     */
    public void loadDataInfile(String filename, String tablesuffix) {
        try {
            System.out.printf(filename + ": " + tablesuffix + "\n");
            if (!checkIsNull(filename, tablesuffix)) {
                String sql = "load data local infile 'c:/outputs/files/yoox/" + filename + "' "
                        + "into table " + tablesuffix + " "
                        + "fields terminated by ',' "
                        + "lines terminated by '\\r\\n' "
                        + "ignore 1 lines;";
                preparedStatement = mysqlConnect().prepareStatement(sql);
                resultSet = preparedStatement.executeQuery();
                //reseting the strings to empty for the next loop
                filename = "";
                tablesuffix = "";
                FileLogger.log("File stored to DB table: " + tablesuffix);
            } else {
                System.err.print(filename + " does not have a key or key is not being accessed.\n");
            }
        } catch (SQLException ex) {
            Logger.getLogger(JavaFTP.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private boolean checkIsNull(String filename, String tablesuffix) {
        return filename.isEmpty() && filename == null && tablesuffix == null && tablesuffix.isEmpty();
    }

}
