/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaftp;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ybarnaby
 */
public class JavaFTP {

    mySQLConnectors mysqlconnectors = new mySQLConnectors();
    private final String HOME_DIR = "c:/outputs/files/";
    private final String ARCHIVE_DIR = "c:/outputs/archivedyoox/";
    public final static String PROPERTIES_LOCATION = "c:/outputs/files/stuff.properties";
    FileLogger logging = new FileLogger();

    List<String> mapFiles = new ArrayList<String>();
    Properties prop = new Properties();

    private static final Logger logger = Logger.getLogger(JavaFTP.class.getName());

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        JavaFTP javaftp = new JavaFTP();
        javaftp.runSequence(); // run this god-like sequence
        
//        appender = new FileAppender(new SimpleLayout(), "c:/outputs/log/JavaFTP-Log.txt");
        /*
        javaftp.getFTPCredentials("pass");
         System.out.print(javaftp.getDirFromFTPHost(yoox_dir));
        Properties p = new Properties();
        FileInputStream input = new FileInputStream("src/javaftp/ftp.properties");
        p.load(input);
        p.getProperty("luck");
        javaftp.getTableProperty();
        javaftp.connectToFtp();
            JavaFTP.downloadDirectory(javaftp.connectToFtp(), "/yoox/", "/", "C:/outputs/files");
       
      JavaFTP.downloadSingleFile(javaftp.connectToFtp(), "/test.csv", "C:/Outputs/other/o/me.csv");
        javaftp.getFiles();
        javaftp.filesToMySQL() ;
        javaftp.loopThruFolder();
        javaftp.lookUpTable("02_stock.csv");
         
         */

    }

    /**
     *
     * @throws IOException God function that calls all the rest
     */
    private void runSequence() throws IOException {
         FileLogger.bLog("\n--begging log.");
        KinFTP kinoftp = new KinFTP(); 
        FileLogger.bLog("1. Connecting to FTP...\n");
        kinoftp.connectToFtp();
        FileLogger.bLog("2. Saving files to disk...\n");
        downloadDirectory(kinoftp.connectToFtp(), "/yoox/", "", HOME_DIR); //to get files from yoox
        FileLogger.bLog("3. Putting files in DB...\n");
        loopThruFilesInFolder(); // loops and drops files in directory.

        FileLogger.bLog("----Files saved successfully!----\n");
        FileLogger.bLog("\n--ending log.");
    }

    /**
     * retrieves specific folders based on FTP host i.e, yoox downloads to yoox
     * folder, cegid downloads to cigid folder
     */
    /**
     * Download a single file from the FTP server
     *
     * @param ftpClient an instance of org.apache.commons.net.host.FTPClient
     * class.
     * @param remoteFilePath path of the file on the server
     * @param savePath path of directory where the file will be stored
     * @return true if the file was downloaded successfully, false otherwise
     * @throws IOException if any network or IO error occurred.
     */
    public boolean downloadSingleFile(FTPClient ftpClient,
            String remoteFilePath, String savePath) throws IOException {
        File downloadFile = new File(savePath);

        File parentDir = downloadFile.getParentFile();
        if (!parentDir.exists()) {
            parentDir.mkdir();
        }

        OutputStream outputStream = new BufferedOutputStream(
                new FileOutputStream(downloadFile));
        try {
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            return ftpClient.retrieveFile(remoteFilePath, outputStream);
        } catch (IOException ex) {
            throw ex;
        } finally {
            outputStream.close();
        }
    }

    /**
     * Download a whole directory from a FTP server.
     *
     * @param ftpClient an instance of org.apache.commons.net.host.FTPClient
     * class.
     * @param parentDir Path of the parent directory of the current directory
     * being downloaded.
     * @param currentDir Path of the current directory being downloaded.
     * @param saveDir path of directory where the whole remote directory will be
     * downloaded and saved.
     * @throws IOException if any network or IO error occurred.
     */
    public void downloadDirectory(FTPClient ftpClient, String parentDir,
            String currentDir, String saveDir) throws IOException {
        String dirToList = parentDir;
        if (!currentDir.equals("")) {
            dirToList += "/" + currentDir;
        }

        FTPFile[] subFiles = ftpClient.listFiles(dirToList);

        if (subFiles != null && subFiles.length > 0) {
            for (FTPFile aFile : subFiles) {
                String currentFileName = aFile.getName();
                if (currentFileName.equals(".") || currentFileName.equals("..")) {
                    // skip parent directory and the directory itself
                    continue;
                }
                String filePath = parentDir + "/" + currentDir + "/"
                        + currentFileName;
                if (currentDir.equals("")) {
                    filePath = parentDir + "/" + currentFileName;
                }

                String newDirPath = saveDir + parentDir + File.separator
                        + currentDir + File.separator + currentFileName;
                if (currentDir.equals("")) {
                    newDirPath = saveDir + parentDir + File.separator
                            + currentFileName;
                }

                if (aFile.isDirectory()) {
                    // create the directory in saveDir
                    File newDir = new File(newDirPath);
                    boolean created = newDir.mkdirs();
                    if (created) {
                        System.out.println("CREATED the directory: " + newDirPath);
//                        FileLogger.bLog("CREATED the directory: " + newDirPath);
                    } else {
                        System.out.println("COULD NOT create the directory: " + newDirPath);
                    }

                    // download the sub directory
                    downloadDirectory(ftpClient, dirToList, currentFileName,
                            saveDir);
                } else {
                    // download the file
                    boolean success = downloadSingleFile(ftpClient, filePath,
                            newDirPath);
                    if (success) {
                        System.out.println("DOWNLOADED the file: " + filePath);

                    } else {
                        System.out.println("COULD NOT download the file: "
                                + filePath);
                    }
                }
            }
        }
    }

    /**
     * This loops through all of the files in the directory
     *
     */
    private void loopThruFilesInFolder() {
        File dir = new File(HOME_DIR);
        File[] directoryListing = dir.listFiles();

        if (directoryListing != null) {
            for (File child : directoryListing) {
                // Do something with child
                if (child.getName().endsWith("csv")) {
                    File newDir = new File(ARCHIVE_DIR/*"c:/outputs/archiveyoox/" */ + child.getName());
//                lookUpTable(child.getName());
                    // System.out.print(child.getName()+"\n");
                    //logger.log(Level.FINE, "Filename: {0}", child.getName());
//                    FileLogger.bLog(child.getName());
                    mysqlconnectors.loadDataInfile(child.getName(), getTableProperty(child.getName().toLowerCase()));
                    moveToDir(child, newDir);
                }
            }
        } else {
            System.err.print("No compatible files found");
            logger.log(Level.FINE, "No compatible files found");
            FileLogger.bLog("No compatible files found");

        }
    }

    /**
     *
     * @param path
     * @param endsWith
     */
    void loopThru(String path, String endsWith) {
        File dir = new File(path);
        File[] directoryListing = dir.listFiles();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                if (child.getName().endsWith(endsWith)) {
                    //TODO: change the archive directory to the proper ones
                    File newDir = new File("C:/outputs/archiveyoox/" + child.getName());
//                  System.out.println(child.getName());
                    //logger.log(Level.FINE, "Get all properties: {0}", child.getName());
//                    FileLogger.bLog("Loop Through output: " + child.getName());
                    mapFiles.add(child.getName());
                    mysqlconnectors.loadDataInfile(child.getName(), getTableProperty(child.getName()), getPrefix(HOME_DIR).toLowerCase());
                    moveToDir(child, newDir);
                }
            }
        }
    }

    String getTableProperty(String name) {
        try {
            FileInputStream input = new FileInputStream(PROPERTIES_LOCATION);
            prop.load(input);
            // logger.log(Level.FINE, prop.getProperty(name));
            FileLogger.log("Get table property: " + prop.getProperty(name));
            System.out.println(prop.getProperty(name));
            return prop.getProperty(name);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JavaFTP.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(JavaFTP.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     *
     * @param directory
     * @return string prefix name from directory path
     */
    String getPrefix(String directory) {
        //logger.log(Level.FINE, "Got prefix: {0}", prop.getProperty(directory));
        FileLogger.log("Get Prefix: " + prop.getProperty(directory));
        return prop.getProperty(directory);
    }

    /**
     *
     * @param filename
     * @param dir move files from the active directory to an archive
     */
    void moveToDir(File filename, File dir) {
        try {
            Files.move(filename.toPath(), dir.toPath(), ATOMIC_MOVE);
            FileLogger.log("Moved file location: " + dir.toPath().toString());
        } catch (IOException ex) {
            Logger.getLogger(JavaFTP.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
