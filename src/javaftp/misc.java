/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaftp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ybarnaby
 */
public class misc {

    private Statement statement;
//    private ResultSet resultSet;
    
    /**
 * Not sure if I'm going to use this.
 * @param resultSet
 * @throws SQLException 
 */
    private void writeResultSet(ResultSet resultSet) throws SQLException {
        // ResultSet is initially before the first data set
        while (resultSet.next()) {
            // It is possible to get the columns via name
            // also possible to get the columns via the column number
            // which starts at 1
            // e.g. resultSet.getSTring(2);
            String awstore = resultSet.getString("awstore");
            String itemcode = resultSet.getString("itemcode");
            String netSales = resultSet.getString("NetSales");
            Date date = resultSet.getDate("cdate");
            String saleQty = resultSet.getString("SalesQty");
            System.out.println("AW Store: " + awstore);
            System.out.println("Item Code: " + itemcode);
            System.out.println("Net Sales: " + netSales);
            System.out.println("Date: " + date);
            System.out.println("Sales Qty: " + saleQty);
        }
    }
    
    void filesToMySQL() {
        ResultSet resultSet = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            Connection connect = DriverManager.getConnection("jdbc:mysql://app2/diinfo?" + "user=ybarnaby&password=!");

            statement = connect.createStatement();
            resultSet = statement.executeQuery("select * from itemizedList2;");
            //writeResultSet(resultSet);

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(JavaFTP.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    
        /**
     * creates the tables - deprecated
     */
    /*void mysqlCreateTable() {
        try {
            String sql = "Create table if not exist salesandreturns;";
            statement = mysqlConnect().createStatement();
            resultSet = statement.executeQuery(sql);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /**
     *
     * @param company
     * @param table
     */
    /*
    void mysqlInsert(String company, String table) {
        try {
            String sql = "insert into " + company + "_" + table + "values(?,?,?,?,?,?,?);";
            preparedStatement = mysqlConnect().prepareStatement(sql);
            preparedStatement.setString(1, "Test");
            preparedStatement.setString(2, "TestEmail");
            preparedStatement.setString(3, "TestWebpage");
            preparedStatement.setDate(4, new java.sql.Date(2009, 12, 11));
            preparedStatement.setString(5, "TestSummary");
            preparedStatement.setString(6, "TestComment");
            statement = mysqlConnect().createStatement();
            resultSet = statement.executeQuery(sql);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    */
}
