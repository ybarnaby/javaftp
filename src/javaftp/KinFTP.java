/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaftp;

import java.io.FileInputStream;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.net.ftp.FTPClient;

/**
 *
 * @author ybarnaby
 */
public class KinFTP {

    //private final String propertiesLocation = "src/resources/stuff.properties";
    // private final String yoox_dir = HOME_DIR + "yoox/";
    FileLogger logging = new FileLogger();

    List<String> mapFiles = new ArrayList<String>();
    Properties prop = new Properties();

    private static final Logger logger = Logger.getLogger(JavaFTP.class.getName());

    public String getDirFromFTPHost(String thisFTPHost) {
        System.out.println(prop.getProperty(thisFTPHost));
        // logger.log(Level.FINEST, "Getting dir from host: {0}", prop.getProperty(thisFTPHost));
//        FileLogger.bLog("KinFTP -> getDirFromFTPHost: "+prop.getProperty(thisFTPHost));
        return prop.getProperty(thisFTPHost);
    }

    public String getFTPCredentials(String suffix) {
        try {
            this.prop = new Properties();
            String str = null;
            FileInputStream input = new FileInputStream(JavaFTP.PROPERTIES_LOCATION);
            prop.load(input);
            Enumeration em = prop.keys();
            while (em.hasMoreElements()) {
                str = (String) em.nextElement();
                if (str.contains("ftp") && str.endsWith(suffix)) {
                    String output = prop.getProperty(str);
//                    FileLogger.bLog(" ---getFTPCredentials:   "+output);
                   // System.out.println(output);
                    return output;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(JavaFTP.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(JavaFTP.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     *
     * @return FTPClient - function to be used repeatedly
     */
    public FTPClient connectToFtp() {
        String user = getFTPCredentials("user");
        String host = getFTPCredentials("host");
//        FileLogger.bLog("===>Logging for host:   "+host);
        //System.out.println(getFTPCredentials("port"));
        int port = Integer.parseInt(getFTPCredentials("port"));
        String pass = getFTPCredentials("pass");

        FTPClient ftpClient = new FTPClient();

        try {
            ftpClient.connect(host, port);
            ftpClient.login(user, pass);
            ftpClient.enterLocalPassiveMode();
            ftpClient.enterLocalActiveMode();
            ftpClient.setFileType(org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE);
            //logger.log(Level.FINE, "FTP Connection: {0}", ftpClient.getStatus());
//            FileLogger.bLog(ftpClient.getStatus());
            return ftpClient;

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(21);
        }
        return null;
    }

}
